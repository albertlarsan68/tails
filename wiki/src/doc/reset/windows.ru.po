# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-09-04 09:56+0200\n"
"PO-Revision-Date: 2021-07-17 19:05+0000\n"
"Last-Translator: Ed Medvedev <edward.medvedev@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Resetting a USB stick using Windows\"]]\n"
msgstr "[[!meta title=\"Стирание флешки в Windows\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/reset.intro\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/reset.intro.ru\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"bug\">\n"
msgstr "<div class=\"bug\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<strong>The following instructions do not work on Windows XP.</strong><br/>\n"
"The version of <span class=\"application\">Diskpart</span> on Windows XP does not list removable disks.\n"
msgstr ""
"<strong>Описанные ниже рекомендации не работают в Windows XP.</strong><br/>\n"
"Версия утилиты <span class=\"application\">Diskpart</span> в Windows XP не отображает сменные носители.\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Title =
#, no-wrap
msgid "Using <span class=\"application\">Diskpart</span>"
msgstr "Использование <span class=\"application\">DiskPart</span>"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<strong>You might overwrite any hard disk on the computer.</strong><br/>\n"
"If at some point you are not sure about the disk number, stop proceeding.\n"
msgstr ""
"<strong>Вы можете перезаписать любой диск на компьютере.</strong><br/>\n"
"Если в какой-то момент вы почувствуете неуверенность в том, с каким диском работать, лучше остановиться.\n"

#. type: Bullet: '1.  '
msgid "Make sure that the USB stick that you want to reset is unplugged."
msgstr ""
"Убедитесь, что флешка, которую вы хотите стереть, не подключена к компьютеру."

#. type: Bullet: '1.  '
#, fuzzy
#| msgid ""
#| "Click on the <span class=\"bold\">Start</span> button, and choose <span "
#| "class=\"menuchoice\">All Programs&nbsp;▸ Accessories&nbsp;▸ Command "
#| "Prompt</span>, to open the <span class=\"application\">[[!wikipedia cmd."
#| "exe desc=\"Command Prompt\"]]</span>,"
msgid ""
"Click on the <span class=\"bold\">Start</span> button, and choose <span "
"class=\"menuchoice\">All Programs&nbsp;▸ Accessories&nbsp;▸ Command Prompt</"
"span>, to open the <span class=\"application\">[[!wikipedia cmd.exe "
"desc=\"Command Prompt\"]]</span>,"
msgstr ""
"Нажмите кнопку <span class=\"bold\">Пуск</span> и выберите <span "
"class=\"menuchoice\">Служебные - Windows&nbsp;▸ Командная строка</span>. "
"Откроется терминал с <span class=\"application\">[[!wikipedia cmd.exe "
"desc=\"командной строкой\"]]</span>,"

#. type: Plain text
#, no-wrap
msgid ""
"    [[More help on how to start the <span class=\"application\">Command\n"
"    Prompt</span>|http://www.computerhope.com/issues/chdos.htm]]\n"
msgstr "    [[Подробнее о том, как запустить <span class=\"application\">командную строку</span>|https://www.computerhope.com/issues/chdos.htm]\n"

#. type: Plain text
#, no-wrap
msgid ""
"2.  Execute the <span class=\"command\">diskpart</span> command, to start\n"
"<span class=\"application\">Diskpart</span>.\n"
msgstr ""
"2.  Для запуска утилиты <span class=\"application\">DiskPart</span> \n"
"выполните команду <span class=\"command\">diskpart</span>.\n"

#. type: Bullet: '3.  '
msgid ""
"Execute the <span class=\"command\">list disk</span> command to obtain "
"information about each disk in the computer."
msgstr ""
"Для вывода информации о всех дисках на компьютере выполните команду <span "
"class=\"command\">list disk</span>."

#. type: Plain text
#, no-wrap
msgid "    For example:\n"
msgstr "    Например:\n"

#. type: Plain text
#, no-wrap
msgid "        Diskpart> list disk\n"
msgstr "        Diskpart> list disk\n"

#. type: Plain text
#, no-wrap
msgid ""
"          Disk ###  Status      Size     Free     Dyn  Gpt\n"
"          --------  ----------  -------  -------  ---  ---\n"
"          Disk 0    Online        80 GB      0 B\n"
msgstr ""
"          Диск ###  Состояние      Размер     Свободно     Дин  GPT\n"
"          --------  ----------  -------  -------  ---  ---\n"
"          Диск 0    В сети        80 Gбайт      0 байт\n"

#. type: Bullet: '4. '
#, fuzzy
#| msgid ""
#| "Plug the USB stick that you want to reset. Run the <span "
#| "class=\"command\">list disk</span> command again."
msgid ""
"Plug in the USB stick that you want to reset. Run the <span "
"class=\"command\">list disk</span> command again."
msgstr ""
"Подключите флешку, которую собираетесь стереть. Снова выполните команду "
"<span class=\"command\">list disk</span>."

#. type: Plain text
#, no-wrap
msgid "   A new disk, which corresponds to that USB stick, appears in the list.\n"
msgstr "   В списке появился новый диск – флешка.\n"

#. type: Plain text
#, no-wrap
msgid "   For example:\n"
msgstr "   Пример:\n"

#. type: Plain text
#, no-wrap
msgid ""
"          Disk ###  Status      Size     Free     Dyn  Gpt\n"
"          --------  ----------  -------  -------  ---  ---\n"
"          Disk 0    Online        80 GB      0 B\n"
"          Disk 1    Online         8 GB      0 B\n"
msgstr ""
"          Диск ###  Состояние      Размер     Свободно     Дин  GPT\n"
"          --------  ----------  -------  -------  ---  ---\n"
"          Диск 0    В сети        80 Gбайт      0 байт\n"
"          Диск 1    В сети         8 Gбайт      0 байт\n"

#. type: Plain text
#, no-wrap
msgid ""
"   Make sure that its size corresponds to the size of the USB stick that you want to\n"
"   reset. Note down the disk number assigned by <span\n"
"   class=\"application\">Diskpart</span> to the USB stick.\n"
msgstr ""
"   Убедитесь, что размер этого диска совпадает с объёмом флешки, которую нужно стереть. Запомните номер диска, который присвоила флешке утилита  <span\n"
"   class=\"application\">DiskPart</span>.\n"

#. type: Bullet: '5. '
msgid ""
"To select the USB stick, execute the following command: <span "
"class=\"command\">select disk=<span class=\"replaceable\">number</span></"
"span>.  Replace <span class=\"replaceable\">number</span> by the disk number "
"of the USB stick that you want to reset."
msgstr ""
"Для выбора флешки выполните следующую команду: <span "
"class=\"command\">select disk=<span class=\"replaceable\">number</span></"
"span>. Замените <span class=\"replaceable\">number</span> на номер флешки, "
"которую нужно стереть."

#. type: Bullet: '6. '
msgid ""
"Execute the <span class=\"command\">clean</span> command to delete the "
"partition table from the USB stick."
msgstr ""
"Выполните команду <span class=\"command\">clean</span> для удаления таблицы "
"разделов с флешки."

#. type: Bullet: '7. '
msgid ""
"Execute the <span class=\"command\">convert mbr</span> command to create a "
"new partition table on the USB stick."
msgstr ""
"Выполните команду <span class=\"command\">convert mbr</span> для создания на "
"флешке новой таблицы разделов."

#. type: Bullet: '8. '
msgid ""
"Execute the <span class=\"command\">create partition primary</span> command "
"to create a new primary partition on the USB stick."
msgstr ""
"Выполните команду <span class=\"command\">create partition primary</span> "
"для создания на флешке главного раздела."

#. type: Title =
#, no-wrap
msgid "Troubleshooting"
msgstr "Решение проблем"

#. type: Plain text
msgid ""
"See the [[Diskpart documentation from Microsoft Support|http://support."
"microsoft.com/kb/300415]]."
msgstr ""
"См. [[документацию по DiskPart от службы поддержки Microsoft|https://docs."
"microsoft.com/ru-ru/windows-server/administration/windows-commands/"
"diskpart]]."

#~ msgid "Using <span class=\"application\">Diskpart</span>\n"
#~ msgstr "Использование <span class=\"application\">DiskPart</span>\n"

#~ msgid "Troubleshooting\n"
#~ msgstr "Решение проблем\n"
