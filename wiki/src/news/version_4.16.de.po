# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-24 19:56+0000\n"
"PO-Revision-Date: 2023-12-05 10:42+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 4.16 is out\"]]\n"
msgstr "[[!meta title=\"Tails 4.16 is out\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Mon, 22 Feb 2021 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Mon, 22 Feb 2021 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Änderungen und Aktualisierungen</h1>\n"

#. type: Plain text
msgid ""
"- Update *Tor Browser* to [10.0.12](https://blog.torproject.org/new-release-"
"tor-browser-10012)."
msgstr ""
"- Aktualisieren Sie *Tor Browser* auf [10.0.12](https://blog.torproject.org/"
"new-release-tor-browser-10012)."

#. type: Plain text
msgid ""
"- Update *Thunderbird* to [78.7.0](https://www.thunderbird.net/en-US/"
"thunderbird/78.7.0/releasenotes/)."
msgstr ""

#. type: Bullet: '- '
msgid ""
"Update *Linux* to 5.10.13. This should improve the support for newer "
"hardware (graphics, Wi-Fi, and so on)."
msgstr ""

#. type: Plain text
msgid "- Update *Tor* to 0.4.5.5."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"fixes\">Fixed problems</h1>\n"
msgstr "<h1 id=\"fixes\">Behobene Probleme</h1>\n"

#. type: Bullet: '- '
msgid ""
"Stop focusing on the **Cancel** button by default when downloading an "
"upgrade to prevent canceling the download by mistake. ([[!tails_ticket "
"18072]])"
msgstr ""

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog desc=\"changelog"
"\"]]."
msgstr ""
"Für weitere Details lesen Sie bitte unser [[!tails_gitweb debian/changelog "
"desc=\"changelog\"]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr "<h1 id=\"issues\">Bekannte Probleme</h1>\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"news/version_4.14/broken_upgrades\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr "Siehe die Liste der [[Altlasten|support/known_issues]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"get\">Get Tails 4.16</h1>\n"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr ""
"So aktualisieren Sie Ihren Tails USB-Stick und behalten Ihren dauerhaften "
"Speicher"

#. type: Bullet: '- '
msgid ""
"Automatic upgrades are broken from Tails 4.14 and earlier. See the <a href="
"\"#broken-upgrades\">known issue above</a>."
msgstr ""

#. type: Plain text
msgid "- Automatic upgrades are available from Tails 4.15 or later to 4.16."
msgstr ""

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade/#manual]]."
msgstr ""
"Wenn Sie kein automatisches Upgrade durchführen können oder wenn Tails nach "
"einem automatischen Upgrade nicht startet, versuchen Sie bitte ein [["
"manuelles Upgrade|doc/upgrade/#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "So installieren Sie Tails auf einem neuen USB-Stick"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Folgen Sie unserer Installationsanleitung:"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Installieren von Windows|install/windows]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Installieren von macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Installieren von Linux|install/linux]]"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>Der persistente Speicher auf dem USB-Stick geht "
"verloren, \n"
"wenn Sie die Installation statt des Upgrades durchführen.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Nur zum Herunterladen"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 4.16 directly:"
msgstr ""

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Für USB-Sticks (USB-Image)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Für DVDs und virtuelle Maschinen (ISO-Image)|install/download-iso]]"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"next\">What's coming up?</h1>\n"
msgstr ""

#. type: Plain text
msgid "Tails 4.17 is [[scheduled|contribute/calendar]] for March 23."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"We need your help and there are many ways to [[contribute to\n"
"Tails|contribute]] (<a href=\"https://tails.net/donate/?r=4.16\">donating</a> is only one of\n"
"them). Come [[talk to us|about/contact#tails-dev]]!\n"
msgstr ""

#~ msgid ""
#~ "Have a look at our [[!tails_roadmap]] to see where we are heading to."
#~ msgstr ""
#~ "Werfen Sie einen Blick auf die [[!tails_roadmap desc=\"Roadmap\"]], um zu "
#~ "sehen, was wir als Nächstes vorhaben."
