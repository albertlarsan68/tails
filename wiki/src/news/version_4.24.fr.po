# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-24 19:56+0000\n"
"PO-Revision-Date: 2023-11-08 09:11+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 4.24 is out\"]]\n"
msgstr "[[!meta title=\"Tails 4.24 est sorti\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Fri, 04 Nov 2021 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Fri, 04 Nov 2021 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Changements et mises à jour</h1>\n"

#. type: Plain text
msgid ""
"- Update *Tor Browser* to [11.0](https://blog.torproject.org/new-release-tor-"
"browser-11-0)."
msgstr ""
"- Mise à jour du *Navigateur Tor* vers la version [11.0](https://blog."
"torproject.org/new-release-tor-browser-110)."

#. type: Bullet: '- '
msgid ""
"Make the confirmation dialog of *Tails Installer* more scary when "
"reinstalling a USB stick that has a Persistent Storage. ([[!tails_ticket "
"18301]])"
msgstr ""
"La boîte de dialogue de confirmation du *Programme d'installation de Tails* "
"a été rendu plus effrayante lors de la réinstallation d'une clé USB qui "
"possède un stockage persistant. ([[!tails_ticket 18301]])"

#. type: Title ##
#, no-wrap
msgid "Improvements to the Tor Connection assistant"
msgstr "Améliorations de l'assistant de connexion à Tor"

#. type: Plain text
msgid "- Improve the time zone selection interface. ([[!tails_ticket 18514]])"
msgstr ""
"- Amélioration de l'interface de sélection du fuseau horaire. ([[!"
"tails_ticket 18514]])"

#. type: Plain text
msgid ""
"- Improve the explanation when fixing the clock. ([[!tails_ticket 18572]])"
msgstr ""
"- Amélioration de l'explication lors du réglage de l'horloge. ([[!"
"tails_ticket 18572]])"

#. type: Plain text
msgid "- Explain better how to type a bridge. ([[!tails_ticket 18597]])"
msgstr ""
"- Meilleur explication de comment saisir un pont. ([[!tails_ticket 18597]])"

#. type: Plain text
msgid ""
"- Remove mention to local networks when opening the *Unsafe Browser*. ([[!"
"tails_ticket 18600]])"
msgstr ""
"- Suppression de la mention des réseaux locaux lors de l'ouverture du "
"*Navigateur non sécurisé*. ([[!tails_ticket 18600]])"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"fixes\">Fixed problems</h1>\n"
msgstr "<h1 id=\"fixes\">Problèmes corrigés</h1>\n"

#. type: Bullet: '- '
msgid ""
"Avoid high CPU usage when getting download progress info in *Tails "
"Upgrader*.  ([[!tails_ticket 18632]])"
msgstr ""
"Éviter une utilisation élevée du CPU lors de l'obtention d'informations "
"relatives à la progression du téléchargement dans le *Tails Upgrader*.  ([[!"
"tails_ticket 18632]])"

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog desc=\"changelog"
"\"]]."
msgstr ""
"Pour plus de détails, lisez notre [[!tails_gitweb debian/changelog desc="
"\"liste des changements\"]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr "<h1 id=\"issues\">Problèmes connus</h1>\n"

#. type: Bullet: '- '
msgid ""
"The *Unsafe Browser* has display problems, but still works to sign in to a "
"network using a captive portal."
msgstr ""
"Le *Navigateur non sécurisé* a de nombreux problèmes d'affichage, mais "
"fonctionne toujours pour se connecter à un réseau utilisant un portail "
"captif."

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr ""
"Voir la liste des [[problèmes connus de longue date|support/known_issues]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"get\">Get Tails 4.24</h1>\n"
msgstr "<h1 id=\"get\">Obtenir Tails 4.24</h1>\n"

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr ""
"Pour mettre à jour votre clé USB Tails et conserver votre stockage persistant"

#. type: Plain text
msgid "- Automatic upgrades are broken from Tails 4.14 and earlier."
msgstr ""
"- Les mises à jour automatiques sont cassées depuis Tails 4.14 et les "
"versions plus anciennes."

#. type: Plain text
#, no-wrap
msgid ""
"  Follow our instructions to do an [[automatic upgrade from Tails 4.15, Tails\n"
"  4.16, Tails 4.17, or Tails 4.18|doc/upgrade/error/check#4.18]].\n"
msgstr ""
"  Suivez nos instructions pour faire une [mise à jour automatique depuis Tails 4.15, Tails\n"
"  4.16, Tails 4.17, ou Tails 4.18|doc/upgrade/error/check#4.18]].\n"

#. type: Plain text
msgid "- Automatic upgrades are available from Tails 4.19 or later to 4.24."
msgstr ""
"Les mises à jour automatiques sont disponibles depuis Tails 4.19 ou plus "
"récent vers la version 4.24."

#. type: Plain text
#, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Vous pouvez [[réduire la taille du téléchargement|doc/upgrade#reduce]] des futures\n"
"  mises à jours automatiques en effectuant une mise à jour manuelle vers la dernière version.\n"

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade/#manual]]."
msgstr ""
"Si vous ne pouvez pas faire une mise à jour automatique ou si le démarrage "
"de Tails échoue après une mise à jour automatique, merci d'essayer de faire "
"une [[mise à jour manuelle|doc/upgrade/#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "Pour installer Tails sur une nouvelle clé USB"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Suivez nos instructions d'installation :"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Installer depuis Windows|install/windows]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Installer depuis macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Installer depuis Linux|install/linux]]"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>Le stockage persistant de la clé USB sera perdu si\n"
"vous faites une installation au lieu d'une mise à jour.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Pour seulement télécharger"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 4.24 directly:"
msgstr ""
"Si vous n'avez pas besoin d'instructions d'installation ou de mise à jour, "
"vous pouvez télécharger directement Tails 4.24 :"

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Pour clés USB (image USB)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Pour DVD et machines virtuelles (image ISO)|install/download-iso]]"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"next\">What's coming up?</h1>\n"
msgstr "<h1 id=\"next\">Et ensuite ?</h1>\n"

#. type: Plain text
msgid "Tails 4.25 is [[scheduled|contribute/calendar]] for December 7."
msgstr "Tails 4.25 est [[prévu|contribute/calendar]] pour le 7 décembre."

#~ msgid ""
#~ "Have a look at our [[!tails_roadmap]] to see where we are heading to."
#~ msgstr ""
#~ "Jetez un œil à notre [[feuille de route|contribute/roadmap]] pour savoir "
#~ "ce que nous avons en tête."
