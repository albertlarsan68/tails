# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-09-04 09:56+0200\n"
"PO-Revision-Date: 2023-12-05 10:43+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 5.15.1\"]]\n"
msgstr "[[!meta title=\"Tails 5.15.1\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Tue, 11 Jul 2023 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Tue, 11 Jul 2023 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Änderungen und Aktualisierungen</h1>\n"

#. type: Plain text
msgid ""
"- Update *Tor Browser* to [12.5](https://blog.torproject.org/new-release-tor-"
"browser-125)."
msgstr ""
"- Aktualisieren Sie *Tor Browser* auf [12.5](https://blog.torproject.org/new-"
"release-tor-browser-125)."

#. type: Plain text
#, no-wrap
msgid ""
"  [Tor Browser 12.5](https://blog.torproject.org/new-release-tor-browser-125)\n"
"  has a new circuit view and better information about onion services.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img doc/anonymous_internet/Tor_Browser/circuit-view.png link=\"no\"]]\n"
msgstr ""

#. type: Plain text
msgid "- Support onion service authentication in *Tor Browser*."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  For example, onion service authentication is used by *OnionShare* outside of\n"
"  Tails.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img authentication.png link=\"no\" alt=\"Tor Browser asking for a private key to connect to an onion service\"]]\n"
msgstr ""

#. type: Plain text
msgid "- Display the version of Tails in the Boot Loader"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img version.png link=\"no\" alt=\"SYSLINUX Boot Loader displaying 'Tails 5.15.1'\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"fixes\">Fixed problems</h1>\n"
msgstr "<h1 id=\"fixes\">Behobene Probleme</h1>\n"

#. type: Plain text
msgid ""
"- Fix the search of some languages and keyboard layouts. ([[!tails_ticket "
"19200]])"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img language.png link=\"no\" alt=\"Searching for 'esp' now returns 'Español'\"]]\n"
msgstr ""

#. type: Plain text
msgid "- Make the upgrade of the Persistent Storage more reliable:"
msgstr "- Die Aufrüstung des persistenten Speichers wird zuverlässiger:"

#. type: Bullet: '  * '
msgid ""
"On some systems, the message \"*Upgrading the persistent storage*\" was "
"displayed every time in Tails 5.14 when Tails failed to upgrade one of the "
"cryptographic parameters. ([[!tails_ticket 19734]])"
msgstr ""

#. type: Bullet: '  * '
msgid ""
"On some USB sticks, upgrading the Persistent Storage was too slow and failed "
"with the error message \"*Upgrade of persistent storage failed*\".  ([[!"
"tails_ticket 19728]])"
msgstr ""

#. type: Plain text
msgid ""
"- Fix opening documentation links from *Tails Installer*. ([[!tails_ticket "
"19870]])"
msgstr ""

#. type: Bullet: '- '
msgid ""
"Prevent *Tor Browser* to access information about other Tor circuits.  ([[!"
"tails_ticket 19740]])"
msgstr ""

#. type: Bullet: '- '
msgid ""
"Make the error when scanning the QR code of a bridge more consistent.  ([[!"
"tails_ticket 19737]])"
msgstr ""

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog "
"desc=\"changelog\"]]."
msgstr ""
"Für weitere Details lesen Sie bitte unser [[!tails_gitweb debian/changelog "
"desc=\"changelog\"]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr "<h1 id=\"issues\">Bekannte Probleme</h1>\n"

#. type: Plain text
msgid ""
"[[!tails_ticket 19728]] was affecting 5.14, and it might still be present. "
"If you encounter this bug, please [[send us a *WhisperBack* report|doc/"
"first_steps/bug_reporting/#whisperback]]."
msgstr ""

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr "Siehe die Liste der [[Altlasten|support/known_issues]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"get\">Get Tails 5.15.1</h1>\n"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr ""
"So aktualisieren Sie Ihren Tails USB-Stick und behalten Ihren dauerhaften "
"Speicher"

#. type: Plain text
msgid "- Automatic upgrades are available from Tails 5.0 or later to 5.15.1."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Sie können [[die Größe des Downloads|doc/upgrade#reduce]] von zukünftigen\n"
"  automatischen Upgrades reduzieren, indem Sie ein manuelles Upgrade auf die "
"neueste Version durchführen.\n"

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade/#manual]]."
msgstr ""
"Wenn Sie kein automatisches Upgrade durchführen können oder wenn Tails nach "
"einem automatischen Upgrade nicht startet, versuchen Sie bitte ein [["
"manuelles Upgrade|doc/upgrade/#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "So installieren Sie Tails auf einem neuen USB-Stick"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Folgen Sie unserer Installationsanleitung:"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Installieren von Windows|install/windows]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Installieren von macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Installieren von Linux|install/linux]]"

#. type: Bullet: '  - '
msgid ""
"[[Install from Debian or Ubuntu using the command line and GnuPG|install/"
"expert]]"
msgstr ""
"[[Installation von Debian oder Ubuntu über die Kommandozeile und GnuPG|"
"install/expert]]"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>Der persistente Speicher auf dem USB-Stick geht "
"verloren, \n"
"wenn Sie die Installation statt des Upgrades durchführen.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Nur zum Herunterladen"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 5.15.1 directly:"
msgstr ""

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Für USB-Sticks (USB-Image)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Für DVDs und virtuelle Maschinen (ISO-Image)|install/download-iso]]"
