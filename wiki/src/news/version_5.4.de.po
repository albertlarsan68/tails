# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-08-24 19:56+0000\n"
"PO-Revision-Date: 2023-12-05 10:42+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 5.4 is out\"]]\n"
msgstr "[[!meta title=\"Tails 5.4 is out\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Tue, 24 Aug 2022 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Tue, 24 Aug 2022 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Änderungen und Aktualisierungen</h1>\n"

#. type: Bullet: '- '
msgid ""
"Harden several aspects of our *Linux* kernel. ([[!tails_ticket 18302]], [[!"
"tails_ticket 18858]], and [[!tails_ticket 18886]])"
msgstr ""

#. type: Bullet: '- '
msgid ""
"Disable HTTPS-only mode in the *Unsafe Browser* to make it easier to sign in "
"to Wi-Fi networks."
msgstr ""

#. type: Plain text
msgid ""
"- Update *Tor Browser* to [11.5.2](https://blog.torproject.org/new-release-"
"tor-browser-1152)."
msgstr ""

#. type: Plain text
msgid "- Update *tor* to 0.4.7.10."
msgstr ""

#. type: Bullet: '- '
msgid ""
"Update the *Linux* kernel to 5.10.136. This should improve the support for "
"newer hardware: graphics, Wi-Fi, and so on. It will also fix an important "
"vulnerability ([[!tails_gitweb 19081]])"
msgstr ""

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog desc=\"changelog"
"\"]]."
msgstr ""
"Für weitere Details lesen Sie bitte unser [[!tails_gitweb debian/changelog "
"desc=\"changelog\"]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr "<h1 id=\"issues\">Bekannte Probleme</h1>\n"

#. type: Plain text
msgid "None specific to this release."
msgstr "Keine spezifischen Angaben zu dieser Version."

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr "Siehe die Liste der [[Altlasten|support/known_issues]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"get\">Get Tails 5.4</h1>\n"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr ""
"So aktualisieren Sie Ihren Tails USB-Stick und behalten Ihren dauerhaften "
"Speicher"

#. type: Plain text
msgid "- Automatic upgrades are available from Tails 5.0 or later to 5.4."
msgstr ""
"- Automatische Upgrades sind von Tails 5.0 oder höher auf 5.4 verfügbar."

#. type: Plain text
#, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Sie können [[die Größe des Downloads|doc/upgrade#reduce]] von zukünftigen\n"
"  automatischen Upgrades reduzieren, indem Sie ein manuelles Upgrade auf die "
"neueste Version durchführen.\n"

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade/#manual]]."
msgstr ""
"Wenn Sie kein automatisches Upgrade durchführen können oder wenn Tails nach "
"einem automatischen Upgrade nicht startet, versuchen Sie bitte ein [["
"manuelles Upgrade|doc/upgrade/#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "So installieren Sie Tails auf einem neuen USB-Stick"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Folgen Sie unserer Installationsanleitung:"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Installieren von Windows|install/windows]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Installieren von macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Installieren von Linux|install/linux]]"

#. type: Bullet: '  - '
msgid ""
"[[Install from Debian or Ubuntu using the command line and GnuPG|install/"
"expert]]"
msgstr ""
"[[Installation von Debian oder Ubuntu über die Kommandozeile und GnuPG|"
"install/expert]]"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>Der persistente Speicher auf dem USB-Stick geht "
"verloren, \n"
"wenn Sie die Installation statt des Upgrades durchführen.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Nur zum Herunterladen"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 5.4 directly:"
msgstr ""

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Für USB-Sticks (USB-Image)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Für DVDs und virtuelle Maschinen (ISO-Image)|install/download-iso]]"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"next\">What's coming up?</h1>\n"
msgstr ""

#. type: Plain text
msgid "Tails 5.5 is [[scheduled|contribute/calendar]] for September 20."
msgstr ""
