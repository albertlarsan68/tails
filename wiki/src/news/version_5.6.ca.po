# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-08-24 19:56+0000\n"
"PO-Revision-Date: 2023-12-21 00:22+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 5.6 is out\"]]\n"
msgstr "[[!meta title=\"Tails 5.6\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Tue, 25 Oct 2022 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Tue, 25 Oct 2022 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Canvis i actualitzacions</h1>\n"

#. type: Plain text
msgid ""
"- Update *Tor Browser* to [11.5.5](https://blog.torproject.org/new-release-"
"tor-browser-1155)."
msgstr ""
"- S'ha actualitzat el *Navegador Tor* a la versió [11.5.5](https://blog."
"torproject.org/new-release-tor-browser-1155)."

#. type: Bullet: '- '
msgid ""
"Update the *Linux* kernel to 5.10.149. This should improve the support for "
"newer hardware: graphics, Wi-Fi, and so on."
msgstr ""
"S'ha actualitzat el nucli de *Linux* a la versió 5.10.149. Això hauria de "
"millorar la compatibilitat amb el maquinari més nou: gràfics, Wi-Fi, etc."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"fixes\">Fixed problems</h1>\n"
msgstr "<h1 id=\"fixes\">Problemes solucionats</h1>\n"

#. type: Bullet: '- '
msgid ""
"When a manual upgrade is required, Tails now always points to the release "
"notes of the latest version, instead of pointing sometimes to a deprecated "
"version. ([[!tails_ticket 17069]])"
msgstr ""
"Quan es requereix una actualització manual, Tails ara sempre apunta a les "
"notes de llançament de la darrera versió, en lloc d'apuntar de vegades a una "
"versió obsoleta. ([[!tails_ticket 17069]])"

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog desc=\"changelog"
"\"]]."
msgstr ""
"Per a més detalls, llegiu el nostre [[!tails_gitweb debian/changelog desc="
"\"llistat de canvis\"]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr "<h1 id=\"issues\">Problemes coneguts</h1>\n"

#. type: Plain text
msgid "None specific to this release."
msgstr "Cap específic d'aquesta versió."

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr "Vegeu la llista de [[problemes de llarga durada|support/known_issues]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"get\">Get Tails 5.6</h1>\n"
msgstr "<h1 id=\"get\">Obtenir Tails 5.6</h1>\n"

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr ""
"Per actualitzar el vostre llapis USB de Tails i mantenir el vostre "
"Emmagatzematge Persistent"

#. type: Plain text
msgid "- Automatic upgrades are available from Tails 5.0 or later to 5.6."
msgstr ""
"- Les actualitzacions automàtiques estan disponibles des de Tails 5.0 o "
"posterior a la versió 5.6."

#. type: Plain text
#, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Podeu [[reduir la mida de la baixada|doc/upgrade#reduce]] de futures\n"
"  actualitzacions automàtiques fent una actualització manual a la darrera "
"versió.\n"

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade/#manual]]."
msgstr ""
"Si no podeu fer una actualització automàtica o si Tails no s'inicia després "
"d'una actualització automàtica, proveu de fer una [[actualització manual|doc/"
"upgrade/#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "Per instal·lar Tails en un nou llapis USB"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Seguiu les nostres instruccions d'instal·lació:"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Instal·lar des de Windows|install/windows]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Instal·lar des de macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Instal·lar des de Linux|install/linux]]"

#. type: Bullet: '  - '
msgid ""
"[[Install from Debian or Ubuntu using the command line and GnuPG|install/"
"expert]]"
msgstr ""
"[[Instal·lar des de Debian o Ubuntu mitjançant la línia d'ordres i GnuPG|"
"install/expert]]"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>L'Emmagatzematge Persistent del llapis USB es "
"perdrà si\n"
"instal·leu en comptes d'actualitzar.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Per només baixar"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 5.6 directly:"
msgstr ""
"Si no necessiteu instruccions d'instal·lació o actualització, podeu baixar "
"Tails 5.6 directament:"

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Per a llapis USB (imatge USB)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Per a DVD i màquines virtuals (imatge ISO)|install/download-iso]]"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"next\">What's coming up?</h1>\n"
msgstr "<h1 id=\"next\">Què ve?</h1>\n"

#. type: Plain text
msgid "Tails 5.7 is scheduled for November 22."
msgstr "Tails 5.7 està previst per al 22 de novembre."
