# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-12-22 04:29+0000\n"
"PO-Revision-Date: 2023-12-16 13:19+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Known issues with graphics cards\"]]\n"
msgstr "[[!meta title=\"Problemes coneguts amb les targetes gràfiques\"]]\n"

#. type: Plain text
msgid ""
"- For other hardware compatibility issues, refer to our [[known issues|"
"support/known_issues]]."
msgstr ""
"- Per a altres problemes de compatibilitat de maquinari, consulteu els "
"nostres [[problemes coneguts|support/known_issues]]."

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"error-starting-gdm\">Error starting GDM</h1>\n"
msgstr "<h1 id=\"error-starting-gdm\">Error en iniciar GDM</h1>\n"

#. type: Plain text
msgid ""
"This section applies if you see the following error message when starting "
"Tails:"
msgstr ""
"Aquesta secció s'aplica si veieu el missatge d'error següent en iniciar "
"Tails:"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<p class=\"pre\">\n"
#| "Error starting GDM with your graphics card: <i>name of your graphics\n"
#| "card [id] (rev number)</i>. Please take note of this error and visit\n"
#| "https://tails.net/gdm for troubleshooting.\n"
#| "</p>\n"
msgid ""
"<p class=\"code\">\n"
"Error starting GDM with your graphics card: <span class=\"command-placeholder\">name of your graphics\n"
"card [id] (rev number)</span>. Please take note of this error and visit\n"
"https://tails.net/gdm for troubleshooting.\n"
"</p>\n"
msgstr ""
"<p class=\"pre\">\n"
"Error en iniciar GDM amb la vostra targeta gràfica: <i>nom de la vostra\n"
"targeta gràfica [id] (número de rev.)</i>. Si us plau, tingueu en compte aquest error i visiteu\n"
"https://tails.net/gdm per a la resolució de problemes.\n"
"</p>\n"

#. type: Plain text
#, no-wrap
msgid "[[!img error.png link=\"no\" alt=\"\"]]\n"
msgstr "[[!img error.png link=\"no\" alt=\"\"]]\n"

#. type: Bullet: '1. '
msgid ""
"Identify the name, ID, and revision number (if any) of your graphics card."
msgstr ""
"Identifiqueu el nom, l'ID i el número de revisió (si n'hi ha) de la vostra "
"targeta gràfica."

#. type: Plain text
#, no-wrap
msgid "   For example, if your error message starts with:\n"
msgstr "   Per exemple, si el vostre missatge d'error comença amb:\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "   <p class=\"pre\">Error starting GDM with your graphics card:\n"
#| "   NVIDIA Corporation [10de:0a6c] GT218M [NVS 3100M] (rev a2)</p>\n"
msgid "   <p class=\"code\">Error starting GDM with your graphics card: NVIDIA Corporation [10de:0a6c] GT218M [NVS 3100M] (rev a2)</p>\n"
msgstr ""
"   <p class=\"pre\">Error starting GDM with your graphics card:\n"
"   NVIDIA Corporation [10de:0a6c] GT218M [NVS 3100M] (rev a2)</p>\n"

#. type: Bullet: '   - '
msgid "The name is *NVIDIA Corporation GT218M [NVS 3100M]*."
msgstr "El nom és *NVIDIA Corporation GT218M [NVS 3100M]*."

#. type: Bullet: '   - '
msgid ""
"The ID is *[10de:0a6c]*. The ID is unique to the model of your graphics "
"card, it is not unique to your computer."
msgstr ""
"L'identificador és *[10de:0a6c]*. L'identificador és exclusiu del model de "
"la vostra targeta gràfica, no és exclusiu del vostre ordinador."

#. type: Bullet: '   - '
msgid ""
"The revision number is *a2*. Your graphics card might have no revision "
"number."
msgstr ""
"El número de revisió és *a2*. És possible que la vostra targeta gràfica no "
"tingui número de revisió."

#. type: Bullet: '1. '
msgid ""
"Check if your graphics card is listed below. For example, you can search for "
"its name or ID on this page."
msgstr ""
"Comproveu si la vostra targeta gràfica apareix a continuació. Per exemple, "
"podeu cercar el seu nom o identificador en aquesta pàgina."

#. type: Bullet: '   - '
msgid ""
"If your graphics card is listed, check if a workaround is documented to make "
"it work on Tails."
msgstr ""
"Si la vostra targeta gràfica apareix a la llista, comproveu si hi ha una "
"solució alternativa per fer-la funcionar a Tails."

#. type: Plain text
#, no-wrap
msgid ""
"     If the workaround doesn't work, we are sorry that Tails does not work well\n"
"     on this computer. Our best hope is that a future update of Linux drivers will\n"
"     solve the problem.\n"
msgstr ""
"     Si la solució no funciona, lamentem que Tails no funcioni bé\n"
"     en aquest ordinador. La nostra millor esperança és que una futura actualització dels controladors de Linux\n"
"     resolgui el problema.\n"

#. type: Bullet: '   - '
msgid ""
"If your graphics card is not listed, please [[contact our support team by "
"email|support/talk]]."
msgstr ""
"Si la vostra targeta gràfica no apareix a la llista, [[contacteu amb el "
"nostre equip d'assistència per correu electrònic|support/talk]]."

#. type: Plain text
#, no-wrap
msgid "     Mention in your email:\n"
msgstr "     Esmenteu al vostre correu electrònic:\n"

#. type: Bullet: '     * '
msgid "The version of Tails that you are trying to start."
msgstr "La versió de Tails que esteu intentant iniciar."

#. type: Bullet: '     * '
msgid "The name, ID, and revision number (if any) of your graphics card."
msgstr "El nom, ID i número de revisió (si n'hi ha) de la targeta gràfica."

#. type: Plain text
#, no-wrap
msgid "     <div class=\"tip\">\n"
msgstr "     <div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"     <p>You can send us a photo of the error message as it appears on\n"
"     your screen.</p>\n"
msgstr ""
"     <p>Podeu enviar-nos una foto del missatge d'error tal com apareix\n"
"     a la vostra pantalla.</p>\n"

#. type: Plain text
#, no-wrap
msgid "     </div>\n"
msgstr "     </div>\n"

#. type: Bullet: '1. '
msgid ""
"If your problem gets fixed in a future version of Tails, please let us know "
"so we can update this page."
msgstr ""
"Si el vostre problema es soluciona en una versió futura de Tails, feu-nos-ho "
"saber perquè puguem actualitzar aquesta pàgina."

#. type: Plain text
#, no-wrap
msgid "<!--\n"
msgstr "<!--\n"

#. type: Title =
#, no-wrap
msgid "Name and ID in /usr/share/misc/pci.ids"
msgstr "Nom i identificador a /usr/share/misc/pci.ids"

#. type: Plain text
msgid ""
"The correspondence between the name and ID is established in /usr/share/misc/"
"pci.ids."
msgstr ""
"La correspondència entre el nom i l'identificador s'estableix a /usr/share/"
"misc/pci.ids."

#. type: Plain text
msgid "For example:"
msgstr "Per exemple:"

#. type: Plain text
#, no-wrap
msgid ""
"\t8086  Intel Corporation\n"
"\t        0007  82379AB\n"
"\t        [...]\n"
"\t        0046  Core Processor Integrated Graphics Controller\n"
msgstr ""
"\t8086  Intel Corporation\n"
"\t        0007  82379AB\n"
"\t        [...]\n"
"\t        0046  Core Processor Integrated Graphics Controller\n"

#. type: Plain text
msgid "Corresponds to:"
msgstr "Correspon a:"

#. type: Plain text
#, no-wrap
msgid "\tIntel Corporation Core Processor Integrated Graphics Controller [8086:0046]\n"
msgstr "\tIntel Corporation Core Processor Integrated Graphics Controller [8086:0046]\n"

#. type: Title =
#, no-wrap
msgid "Template for new section"
msgstr "Plantilla per a la nova secció"

#. type: Plain text
#, no-wrap
msgid "<a id=\"$ANCHOR\"></a>\n"
msgstr "<a id=\"$ANCHOR\"></a>\n"

#. type: Title -
#, no-wrap
msgid "$FAMILY_NAME"
msgstr "$FAMILY_NAME"

#. type: Plain text
msgid "$LT!-- Issues: #XXXXX #XXXXX --$GT"
msgstr "$LT!-- Problemes: #XXXXX #XXXXX --$GT"

#. type: Title ###
#, no-wrap
msgid "Affected graphics cards"
msgstr "Targetes gràfiques afectades"

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>$VENDOR $DEVICE</td><td>[$VENDOR_ID:$PRODUCT_ID]</td><td>(rev $REVISION_NUMBER)</td></tr>\n"
"</table>\n"
msgstr ""
"<table>\n"
"<tr><th>Nom</th><th>Identificador</th><th>Número de revisió</th></tr>\n"
"<tr><td>$VENDOR $DEVICE</td><td>[$VENDOR_ID:$PRODUCT_ID]</td><td>(rev $REVISION_NUMBER)</td></tr>\n"
"</table>\n"

#. type: Title ###
#, no-wrap
msgid "Workaround"
msgstr "Solució temporal"

#. type: Plain text
msgid "$WORKAROUND_IF_ANY"
msgstr "$WORKAROUND_IF_ANY"

#. type: Plain text
#, no-wrap
msgid "-->\n"
msgstr "-->\n"

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"radeon-hd\">AMD Radeon HD</h2>\n"
msgstr "<h2 id=\"radeon-hd\">AMD Radeon HD</h2>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<!--\n"
"Issues: #11095 #12482\n"
"-->\n"
msgstr ""
"<!--\n"
"Issues: #11095 #12482\n"
"-->\n"

#. type: Plain text
msgid "Any graphics cards in the Radeon HD family might be affected."
msgstr ""
"Qualsevol targeta gràfica de la família Radeon HD es pot veure afectada."

#. type: Plain text
msgid ""
"If the computer has another, integrated graphics card, try configuring the "
"computer to use that one instead of the AMD graphics card.  For example, "
"using the following [[boot option|doc/advanced_topics/boot_options]]:"
msgstr ""
"Si l'ordinador té una altra targeta gràfica integrada, proveu de configurar "
"l'ordinador perquè l'utilitzi en lloc de la targeta gràfica AMD. Per "
"exemple, utilitzant la següent [[opció d'arrencada|doc/advanced_topics/"
"boot_options]]:"

#. type: Plain text
#, fuzzy
#| msgid "`modprobe.blacklist=amdgpu`"
msgid "- `modprobe.blacklist=amdgpu`"
msgstr "`modprobe.blacklist=amdgpu`"

#. type: Plain text
#, fuzzy
#| msgid "`modprobe.blacklist=radeon`"
msgid "- `modprobe.blacklist=radeon`"
msgstr "`modprobe.blacklist=radeon`"

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"radeon-r9\">AMD Radeon R9</h2>\n"
msgstr "<h2 id=\"radeon-r9\">AMD Radeon R9</h2>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<!--\n"
"Issues: #12218 #11850\n"
"-->\n"
msgstr ""
"<!--\n"
"Issues: #12218 #11850\n"
"-->\n"

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Hawaii PRO [Radeon R9 290/390]</td><td>[1002:67b1]</td><td></td></tr>\n"
"</table>\n"
msgstr ""
"<table>\n"
"<tr><th>Nom</th><th>Identificador</th><th>Número de revisió</th></tr>\n"
"<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Hawaii PRO [Radeon R9 290/390]</td><td>[1002:67b1]</td><td></td></tr>\n"
"</table>\n"

#. type: Plain text
msgid ""
"Adding `radeon.dpm=0` to the [[boot options|doc/advanced_topics/"
"boot_options]] might fix the issue."
msgstr ""
"Afegir `radeon.dpm=0` a les [[opcions d'arrencada|doc/advanced_topics/"
"boot_options]] podria solucionar el problema."

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"amd-radeon-rx-400\">AMD Radeon RX 400 family</h2>\n"
msgstr "<h2 id=\"amd-radeon-rx-400\">Família AMD Radeon RX 400</h2>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>Radeon RX 480</td><td></td><td></td></tr>\n"
"</table>\n"
msgstr ""
"<table>\n"
"<tr><th>Nom</th><th>Identificador</th><th>Número de revisió</th></tr>\n"
"<tr><td>Radeon RX 480</td><td></td><td></td></tr>\n"
"</table>\n"

#. type: Plain text
msgid ""
"Adding `amdgpu.dc=0` to the [[boot options|doc/advanced_topics/"
"boot_options]] might fix the issue."
msgstr ""
"Afegir `amdgpu.dc=0` a les [[opcions d'arrencada|doc/advanced_topics/"
"boot_options]] podria solucionar el problema."

#. type: Title ###
#, no-wrap
msgid "Other possibly affected graphics cards"
msgstr "Altres targetes gràfiques possiblement afectades"

#. type: Plain text
msgid ""
"Other graphics cards in the [[!wikipedia Radeon_RX_400_series]] might be "
"affected."
msgstr ""
"Altres targetes gràfiques de la [[!wikipedia Radeon_RX_400_series "
"desc=\"sèrie Radeon RX 400\"]] podrien veure's afectades."

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"amd-vega\">AMD Radeon RX Vega family</h2>\n"
msgstr "<h2 id=\"amd-vega\">Família AMD Radeon RX Vega</h2>\n"

#. type: Plain text
msgid ""
"Graphics cards in the [[!wikipedia Radeon_RX_Vega_series]], as found in some "
"AMD Ryzen processors, might be affected."
msgstr ""
"Les targetes gràfiques de la [[!wikipedia Radeon_RX_Vega_series desc=\"sèrie "
"Radeon RX Vega\"]], tal com es troben en alguns processadors AMD Ryzen, "
"podrien veure's afectades."

#. type: Plain text
msgid ""
"Try starting Tails using the following [[boot option|doc/advanced_topics/"
"boot_options]]:"
msgstr ""
"Proveu d'iniciar Tails amb la següent [[opció d'arrencada|doc/"
"advanced_topics/boot_options]]:"

#. type: Plain text
#, no-wrap
msgid "    iommu=soft\n"
msgstr "    iommu=soft\n"

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"intel\">Intel</h2>\n"
msgstr "<h2 id=\"intel\">Intel</h2>\n"

#. type: Plain text
msgid "Various Intel graphics card, including but not limited to:"
msgstr "Diverses targetes gràfiques Intel, que inclouen, entre d'altres:"

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>Intel Corporation TigerLake-LP GT2 [Iris Xe Graphics]</td><td>[8086:9a49]</td><td></td></tr>\n"
"<tr><td>Intel Corporation JasperLake [UHD Graphics]</td><td>[8086:4e55]</td><td></td></tr>\n"
"<tr><td>Intel Corporation Iris Plus Graphics G1 (Ice Lake)</td><td>[8086:8a56]</td><td></td></tr>\n"
"</table>\n"
msgstr ""
"<table>\n"
"<tr><th>Nom</th><th>Identificador</th><th>Número de revisió</th></tr>\n"
"<tr><td>Intel Corporation TigerLake-LP GT2 [Iris Xe Graphics]</td><td>[8086:9a49]</td><td></td></tr>\n"
"<tr><td>Intel Corporation JasperLake [UHD Graphics]</td><td>[8086:4e55]</td><td></td></tr>\n"
"<tr><td>Intel Corporation Iris Plus Graphics G1 (Ice Lake)</td><td>[8086:8a56]</td><td></td></tr>\n"
"</table>\n"

#. type: Plain text
#, no-wrap
msgid "        i915.force_probe=MODEL\n"
msgstr "        i915.force_probe=MODEL\n"

#. type: Plain text
msgid ""
"… replacing *MODEL* with the 4 characters you see after `8086:` on the error "
"message."
msgstr ""
"… substituint *MODEL* amb els 4 caràcters que veieu després de `8086:` al "
"missatge d'error."

#. type: Plain text
msgid ""
"For example, if you see an error about `8086:4c8a`, use this boot option: "
"`i915.force_probe=4c8a`."
msgstr ""
"Per exemple, si veieu un error sobre `8086:4c8a`, utilitzeu aquesta opció "
"d'arrencada: `i915.force_probe=4c8a`."

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"multiple\">Multiple graphics cards</h2>\n"
msgstr "<h2 id=\"multiple\">Múltiples targetes gràfiques</h2>\n"

#. type: Plain text
msgid "If the error message lists multiple graphics cards:"
msgstr "Si el missatge d'error mostra diverses targetes gràfiques:"

#. type: Bullet: '* '
msgid ""
"If possible, plug in the monitor into the motherboard's Intel graphics card, "
"as opposed to the external Nvidia or AMD/ATI graphics card."
msgstr ""
"Si és possible, connecteu el monitor a la targeta gràfica Intel de la placa "
"base, a diferència de la targeta gràfica externa Nvidia o AMD/ATI."

#. type: Bullet: '* '
msgid ""
"Explicitly select one of the two graphics adapters in the BIOS instead of "
"letting the system choose one automatically. If this does not solve the "
"problem, try selecting the other graphics adapter."
msgstr ""
"Seleccioneu explícitament un dels dos adaptadors de gràfics de la BIOS en "
"lloc de deixar que el sistema en triï un automàticament. Si això no resol el "
"problema, proveu de seleccionar l'altre adaptador de gràfics."

#. type: Bullet: '* '
msgid ""
"Try starting Tails using the following [[boot options|doc/advanced_topics/"
"boot_options]].  You should try only one of these boot options per start "
"attempt."
msgstr ""
"Proveu d'iniciar Tails amb les següents [[opcions d'arrencada|doc/"
"advanced_topics/boot_options]]. Només hauríeu de provar una d'aquestes "
"opcions d'arrencada per intent d'inici."

#. type: Bullet: '  - '
msgid "`modprobe.blacklist=nouveau`"
msgstr "`modprobe.blacklist=nouveau`"

#. type: Bullet: '  - '
msgid "`nouveau.modeset=0`"
msgstr "`nouveau.modeset=0`"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"other-issues\">Other issues</h1>\n"
msgstr "<h1 id=\"other-issues\">Altres problemes</h1>\n"

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"black-screen-switchable-graphics\">Black screen with switchable graphics computers</h2>\n"
msgstr "<h2 id=\"black-screen-switchable-graphics\">Pantalla negra amb ordinadors gràfics commutables</h2>\n"

#. type: Plain text
msgid ""
"Some computers with switchable graphics (such as Optimus) fail to choose a "
"video card and end up on a black screen. This has been reported for MacBook "
"Pro 6,2, MacBook Pro 10,1 Retina, MacBook Pro 15-inch (early 2011) and might "
"affect many others."
msgstr ""
"Alguns ordinadors amb gràfics commutables (com Optimus) no trien una targeta "
"de vídeo i acaben en una pantalla negra. Això s'ha informat per a MacBook "
"Pro 6.2, MacBook Pro 10.1 Retina, MacBook Pro de 15 polzades (principis de "
"2011) i podria afectar molts altres."

#. type: Plain text
msgid "There are several possible workarounds for this issue:"
msgstr "Hi ha diverses solucions temporals possibles per a aquest problema:"

#. type: Bullet: '* '
msgid ""
"For the Mac computers, it is possible to use a third-party application, "
"<http://gfx.io/>, to force integrated graphics only through macOS.  Then "
"restart in that special mode that works with Tails."
msgstr ""
"Per als ordinadors Mac, és possible utilitzar una aplicació de tercers, "
"<http://gfx.io/>, per forçar els gràfics integrats només a través de macOS. "
"A continuació, reinicieu en aquest mode especial que funciona amb Tails."

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"qemu\">Virtual machines with <span class=\"application\">virt-manager</span>, <span class=\"application\">libvirt</span> and <span class=\"application\">QEMU</span></h2>\n"
msgstr "<h2 id=\"qemu\">Màquines virtuals amb <span class=\"application\">virt-manager</span>, <span class=\"application\">libvirt</span> i <span class=\"application\">QEMU </span></h2>\n"

#. type: Plain text
#, no-wrap
msgid ""
"See the\n"
"[[dedicated troubleshooting documentation|doc/advanced_topics/virtualization/virt-manager#graphics-issues]]\n"
"about graphics issues in Tails running inside a virtual machine\n"
"with <span class=\"application\">virt-manager</span>.\n"
msgstr ""
"Consulteu la\n"
"[[documentació dedicada a la resolució de problemes|doc/advanced_topics/virtualization/virt-manager#graphics-issues]]\n"
"sobre problemes de gràfics a Tails quan s'executa dins d'una màquina virtual\n"
"amb <span class=\"application\">virt-manager</span>.\n"

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"vmware\">Virtual machines with <span class=\"application\">VMware</span></h2>\n"
msgstr "<h2 id=\"vmware\">Màquines virtuals amb <span class=\"application\">VMware</span></h2>\n"

#. type: Plain text
msgid ""
"To improve support of Tails running inside a virtual machine with *VMware*, "
"install the `open-vm-tools-desktop` software package as [[Additional "
"Software|doc/persistent_storage/additional_software]] in Tails."
msgstr ""
"Per millorar el suport de Tails quan s'executa dins d'una màquina virtual "
"amb *VMware*, instal·leu el paquet de programari `open-vm-tools-desktop` com "
"a [[Programari Addicional|doc/persistent_storage/additional_software]] a "
"Tails."

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"boot-options\">Probing EDD error</h2>\n"
msgstr "<h2 id=\"boot-options\">Error Probing EDD</h2>\n"

#. type: Plain text
msgid ""
"Use these instructions if Tails failed to start and displayed the following "
"error:"
msgstr ""
"Utilitzeu aquestes instruccions si Tails no s'ha pogut iniciar i mostra "
"l'error següent:"

#. type: Plain text
#, no-wrap
msgid "        Probing EDD (edd=off to disable)...\n"
msgstr "        Probing EDD (edd=off to disable)...\n"

#. type: Bullet: '1. '
msgid ""
"Follow [[our guide for editing boot options|doc/advanced_topics/"
"boot_options]]."
msgstr ""
"Seguiu la [[nostra guia per editar les opcions d'arrencada|doc/"
"advanced_topics/boot_options]]."

#. type: Bullet: '1. '
msgid ""
"For the guide's \"Modify the boot options as needed\" step, type `edd=off` "
"to add this option to the command line."
msgstr ""
"Per al pas \"Modifica les opcions d'arrencada segons sigui necessari\" de la "
"guia, escriviu `edd=off` per afegir aquesta opció a la línia d'ordres."

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"display-artifacts-intel\">Display artifact when starting Tails on Intel display</h2>\n"
msgstr "<h2 id=\"display-artifacts-intel\">Mostra l'artefacte quan s'inicia Tails a la pantalla Intel</h2>\n"

#. type: Plain text
msgid ""
"If you see screen tearing and color artifact shift through display with an "
"Intel graphics card, try this:"
msgstr ""
"Si veieu un trencament de la pantalla i un canvi d'artefacte de color a "
"través de la pantalla amb una targeta gràfica Intel, proveu això:"

#. type: Bullet: '1. '
msgid "Right-click on the desktop"
msgstr "Feu clic amb el botó dret a l'escriptori"

#. type: Bullet: '2. '
msgid "Click *Display Settings*"
msgstr "Feu clic a *Paràmetres de la pantalla*"

#. type: Bullet: '3. '
msgid ""
"Change the refresh rate to a different value.<br /> For example, setting the "
"refresh rate to 48 HZ fixed the problem on Tails 5.9 with this hardware: "
"Gemini Lake N4100 (Intel Graphics 600)."
msgstr ""
"Canvieu la freqüència d'actualització a un valor diferent.<br /> Per "
"exemple, establir la freqüència d'actualització a 48 HZ solucionava el "
"problema a Tails 5.9 amb aquest maquinari: Gemini Lake N4100 (Intel Graphics "
"600)."

#, fuzzy, no-wrap
#~ msgid "<h2 id=\"radeon-r5\">AMD Radeon R5</h2>\n"
#~ msgstr "<a id=\"nvidia-pascal\"></a>\n"

#, fuzzy, no-wrap
#~ msgid "<h2 id=\"amd-radeon-rx-5000\">AMD Radeon RX 5000 family</h2>\n"
#~ msgstr "<a id=\"nvidia-pascal\"></a>\n"

#, fuzzy, no-wrap
#~ msgid "<h2 id=\"intel-uhd-750\">Intel UHD 750</h2>\n"
#~ msgstr "<a id=\"nvidia-pascal\"></a>\n"

#, fuzzy, no-wrap
#~ msgid "<h2 id=\"nvidia-kepler\">Nvidia NVE0 family (Kepler)</h2>\n"
#~ msgstr "<a id=\"nvidia-pascal\"></a>\n"

#, fuzzy, no-wrap
#~ msgid "<h2 id=\"nvidia-ampere\">Nvidia RTX 30X0 family (Ampere)</h2>\n"
#~ msgstr "<a id=\"nvidia-pascal\"></a>\n"

#, fuzzy
#~ msgid "<a id=\"amd-vega\"></a>\n"
#~ msgstr "<a id=\"nvidia-pascal\"></a>\n"

#~ msgid "<a id=\"nvidia-maxwell\"></a>\n"
#~ msgstr "<a id=\"nvidia-maxwell\"></a>\n"

#~ msgid "<a id=\"nvidia-pascal\"></a>\n"
#~ msgstr "<a id=\"nvidia-pascal\"></a>\n"

#, fuzzy
#~ msgid "<a id=\"nvidia-turing\"></a>\n"
#~ msgstr "<a id=\"nvidia-pascal\"></a>\n"
